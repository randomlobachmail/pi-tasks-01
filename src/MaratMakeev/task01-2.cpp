#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
int main()
{
	int funt;
	int duim;
	float cm;
	printf("Enter funt,duim:\n");
	scanf("%d,%d", &funt, &duim);
	if (funt < 0 || duim < 0)
		printf("Error 001!\n");
	else
	{
		while (funt != 0)
		{
			--funt;
			duim += 12;
		}
		cm = (float)duim*2.54;
		printf("Long %.02f cm.\n", cm);
	}
	return 0;
}