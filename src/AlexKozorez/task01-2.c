#include <stdio.h>
#include <math.h>

int main(){
	
	int feets = -1, inches = -1;
	float result;

	while(feets < 0 || inches < 0){
		printf("\n Please, enter your height in format FF'DD \n ");
		printf("Example: 45'11 \n ");
		scanf("%d'%d", &feets, &inches);
	}

	result = (feets*12 + inches)*2.54;

	printf(" Your height is %4.2f cm", result);
	
	return 0;	
}