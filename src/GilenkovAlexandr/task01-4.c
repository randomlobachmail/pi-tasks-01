#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define N 10
int main()
{
	int n;
	int i = 0; 
	int j = 0;
    char str[N];
    puts("Write a number: ");
    fgets(str,N, stdin);
    while (str[j])
		j++;
    n = j % 3;
    for (i = 0; i<j; i++)
		{
	     putchar(str[i]);
	     if ((i + 1) % 3 == n)
		 putchar(' ');
	    }
    return 0;
}