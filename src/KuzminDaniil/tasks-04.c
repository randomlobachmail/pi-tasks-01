#include<stdio.h>

int main()
{
	char str[256];
	int i = 0, j = 0, var, buf;
	printf("Enter your Number >> ");
	fgets(str, 256, stdin);
	while (str[i])
	{
		if ((str[i] - '0' > 9 || str[i] - '0' <0) && str[i] != '\n')
		{
			printf("Incorrect Data Entry\n");
			return 1;
		}
		i++;
	}
	i -= 2;
	var = (3 - (i + 1) % 3) % 3;
	str[i + 1] = 0;
	i = 0;
	while (str[i])
	{
		buf = str[i] - '0';
		if (var < 3)
		{
			printf("%d", buf);
			var++;
		}
		else
		{
			printf(" %d", buf);
			var = 1;
		}
		i++;
	}
	puts("");
	return 0;
}