
#include <stdio.h>

int main()
{
	char str[20];
	
	printf("Enter line of number [from 0 to 999999], please:\n");
	fgets(str, 20, stdin);
	
	str[strlen(str)-1] = 0;
	int i = 0;
	int rest = strlen(str) % 3;
	printf("rest = %i\n", rest);
	
	if (rest == 0){
		printf("Your number = %c", str[i]);
		rest = 1;
		i++;
	}

	for (i; i <= strlen(str); i++){
		printf("%c", str[i]);
		if (rest > 0){
			rest--;
		}
		else{
			printf(" ");
			rest = 2;
		}
	}
	return 0;
}

