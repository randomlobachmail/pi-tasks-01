#include <stdio.h>
#include <string.h>

int main()
{
    char line[256];
    int i, size, begin, end;
    
    printf("Enter a line of digits \n");
    fgets(line,256,stdin);
    
    size = strlen(line)-1;
    begin = size % 3;
    end = size-begin;
    
    for (i = 0; i < begin; ++i)
        printf("%c", line[i]);
    if(begin != 0)
        printf(" ");
    
    for (i = 0; i < size; ++i)
        ((i+1)%3 == 0) ? (printf("%c ", line[i+begin])) : (printf("%c", line[i+begin]));
    
    printf(" \n");
    
    return 0;
}
