#include <stdio.h>

int main()
{
	int number = 0, i = 0;
	char str[256];

	printf("Enter a number \n");
	fgets(str, 256, stdin);

	number = (strlen(str) - 1) % 3;
	for (int i = 0; i < strlen(str); i++)
	{
		printf("%d", str[i]);
		if ((i + 1) % 3 == number)
			printf(" ");
	}

	return 0;
}