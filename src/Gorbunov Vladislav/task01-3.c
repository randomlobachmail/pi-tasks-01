#include <stdio.h>

int main()
{
	char str[256];
	int i, k = 0;
	int sum = 0;

	printf("Input the line: ");
	fgets(str, 256, stdin);

	for (i = 0; i < strlen(str); ++i)
	{
		if (str[i] >= '0' && str[i] <= '9')
			k = k * 10 + str[i]-'0';
		else if (k != 0)
		{
			sum = sum + k;
			k = 0;
		}
	}

	printf("Sum is: %d", sum);
	return 0;
}