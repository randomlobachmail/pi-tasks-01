#include<stdio.h>

int main()
{
    char M[256];
    int sum = 0, buf = 0, inNum = 0, i = 0;
    printf("Enter your string >> ");
    fgets(M, 256, stdin);
    while (M[i] != '\0')
    {
        if (M[i] - '0' >= 0 && M[i] - '0' <= 9)
        {
            buf = buf * 10 + (int)(M[i] - '0');
            if (inNum == 0)
                inNum = 1;
        }
        else if (inNum == 1)
        {
            sum += buf;
            buf = 0;      
        }
        i++;
    }
    printf("Sum = %d", sum);
    puts("");
    return 0;
}