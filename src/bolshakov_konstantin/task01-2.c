#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h> 

int main()
{
	int inches, feet;
	float sm;
	printf("Enter your height - inches, feet\n");
	scanf("%d, %d", &inches, &feet);
	if (inches < 0 || feet < 0)
	{
		printf("Enter correctly\n");
		scanf("%d, %d", &inches, &feet);
	}
	inches = inches * 12;
	feet = feet + inches;
	sm = feet * 2.54;
	printf("You height = %f sm\n", sm);
}